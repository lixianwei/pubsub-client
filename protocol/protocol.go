package protocol

const (
	Ping  = "ping"
	Pong  = "pong"
	Pub   = "pub"
	Sub   = "sub"
	Unsub = "unsub"
)

type Message struct {
	Cmd    string   `json:"cmd"`
	Topics []string `json:"topics,omitempty"`
	Data   string   `json:"data,omitempty"`
}
