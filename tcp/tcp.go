package tcp

import (
	"encoding/json"
	"gitlab.com/lixianwei/pubsub-client/protocol"
	"net"
)

type Client struct {
	host string
	conn net.Conn
	decoder *json.Decoder
	encode *json.Encoder
	MsgChan chan protocol.Message
}

func NewClient(host string) (client *Client) {
	client = &Client{host: host,MsgChan:make(chan protocol.Message)}
	client.connect()
	return
}

func (client *Client) connect() {
	conn, err := net.Dial("tcp", client.host)
	if err != nil {
		panic(err)
	}
	client.conn = conn
	client.encode = json.NewEncoder(conn)
	client.decoder = json.NewDecoder(conn)
	go client.handleRead()
}

func (client *Client) Close() error {
	close(client.MsgChan)
	return client.conn.Close()
}

func (client *Client) Ping() error {
	return client.encode.Encode(protocol.Message{Cmd: protocol.Ping})
}

func (client *Client) Pong() error {
	return client.encode.Encode(protocol.Message{Cmd: protocol.Pong})
}

func (client *Client) Sub(topics []string) error {
	return client.encode.Encode(protocol.Message{Cmd: protocol.Sub, Topics: topics})
}

func (client *Client) Unsub(topics []string) error {
	return client.encode.Encode(protocol.Message{Cmd: protocol.Unsub, Topics: topics})
}

func (client *Client) Pub(topics []string, data string) error {
	return client.encode.Encode(protocol.Message{Cmd: protocol.Pub, Topics: topics, Data: data})
}

func (client *Client) Recv() <- chan protocol.Message{
	return client.MsgChan
}

func (client *Client) handleRead() {
	for {
		msg := protocol.Message{}
		if err := client.decoder.Decode(&msg); err != nil {
			// todo log here
			// todo break?
			continue
		}
		client.MsgChan <- msg
	}
}

//func main() {
//	sigChan := make(chan os.Signal)
//	signal.Notify(sigChan, os.Interrupt, os.Kill)
//
//	client := NewTCPClient("localhost:5002")
//	go func() {
//		for {
//			select {
//			case msg := <-client.MsgChan:
//				fmt.Println(msg)
//			case <-time.After(time.Second * 1):
//				_ = client.Ping()
//			}
//		}
//	}()
//
//	<-sigChan
//}