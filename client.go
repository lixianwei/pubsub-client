package pubsub_client

import (
	"errors"
	"fmt"
	"gitlab.com/lixianwei/pubsub-client/protocol"
	"gitlab.com/lixianwei/pubsub-client/tcp"
	"gitlab.com/lixianwei/pubsub-client/ws"
	"net/url"
)

type Client interface {
	Close() error
	Ping() error
	Pong() error
	Sub(topics []string) error
	Unsub(topics []string) error
	Pub(topics []string, data string)error
	Recv() <- chan protocol.Message
}

func NewClient(uri string) Client {
	parsedUrl, err := url.Parse(uri)
	if err != nil {
		panic(err)
	}
	switch parsedUrl.Scheme {
	case "ws":
		return ws.NewClient(parsedUrl.Host)
	case "tcp":
		return tcp.NewClient(parsedUrl.Host)
	default:
		panic(errors.New(fmt.Sprintf("schema %s not support", parsedUrl.Scheme)))
	}
}