package ws

import (
	"github.com/gorilla/websocket"
	"gitlab.com/lixianwei/pubsub-client/protocol"
	"net/url"
)

type Client struct {
	host    string
	conn    *websocket.Conn
	MsgChan chan protocol.Message
}

func NewClient(host string) (client *Client) {
	client = &Client{host: host, MsgChan: make(chan protocol.Message)}
	client.connect()
	return
}

func (client *Client) connect() {
	u := url.URL{Scheme: "ws", Host: client.host, Path: "/upgrade"}
	conn, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		panic(err)
	}
	client.conn = conn
	go client.handleRead()
}

func (client *Client) Close() error {
	close(client.MsgChan)
	return client.conn.Close()
}

func (client *Client) Ping() error {
	return client.conn.WriteJSON(protocol.Message{Cmd: protocol.Ping})
}

func (client *Client) Pong() error {
	return client.conn.WriteJSON(protocol.Message{Cmd: protocol.Pong})
}

func (client *Client) Sub(topics []string) error {
	return client.conn.WriteJSON(protocol.Message{Cmd: protocol.Sub, Topics: topics})
}

func (client *Client) Unsub(topics []string) error {
	return client.conn.WriteJSON(protocol.Message{Cmd: protocol.Unsub, Topics: topics})
}

func (client *Client) Pub(topics []string, data string) error {
	return client.conn.WriteJSON(protocol.Message{Cmd: protocol.Pub, Topics: topics, Data: data})
}

func (client *Client) Recv() <- chan protocol.Message{
	return client.MsgChan
}

func (client *Client) handleRead() {
	for {
		msg := protocol.Message{}
		if err := client.conn.ReadJSON(&msg); err != nil {
			// todo log here
			// todo break?
			continue
		}
		client.MsgChan <- msg
	}
}

//func main() {
//	sigChan := make(chan os.Signal)
//	signal.Notify(sigChan, os.Interrupt, os.Kill)
//
//	client := NewWSClient("localhost:5001")
//	go func() {
//		for {
//			select {
//			case msg := <-client.MsgChan:
//				fmt.Println(msg)
//			case <-time.After(time.Second * 1):
//				_ = client.Ping()
//			}
//		}
//	}()
//
//	<-sigChan
//}
