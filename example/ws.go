package main

import (
	"fmt"
	"gitlab.com/lixianwei/pubsub-client"
	"os"
	"os/signal"
	"time"
)

func main() {
	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, os.Interrupt, os.Kill)
	client := pubsub_client.NewClient("ws://localhost:5001")
	go func() {
		for {
			select {
			case msg:=<-client.Recv():
				fmt.Println(msg)
			case <-time.After(time.Second * 3):
				err := client.Ping()
				if err != nil {
					fmt.Println(err.Error())
				}
			}
		}
	}()
	<-sigChan
	client.Close()
}

